package no.dips.ehrcraft.simplehr.rm;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dom4j.Element;
import org.dom4j.Node;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor @AllArgsConstructor
public class Quantity {
    private double magnitude;
    private String unit;

    public static Quantity create(Node node) {
        return create((Element)node);
    }

    private static Quantity create(Element element){
String type = element.attributeValue("type");
if(type == null){
    throw new RuntimeException("xsi:type is null for element " + element.asXML());
}else{
    if("ELEMENT". contentEquals(type)){
return elementToQuantity(element);
    }else if("DV_QUANTITY".contentEquals(type)){
        return valueToQuantity(element);

    }else{
        throw new RuntimeException("Unsupported type" + type);
    }
}
    }

    private static Quantity valueToQuantity(Element element) {
        String magn =  element.selectSingleNode("oe:magnitude").getStringValue();
        double d = Double.parseDouble(magn);
        String units = element.selectSingleNode("oe:units").getStringValue();
        Quantity q = new Quantity(d, units);
        return q;

    }

    private static Quantity elementToQuantity(Element element) {
        Node n = element.selectSingleNode("oe:value");
        return valueToQuantity((Element) n);
    }

    public static List<Quantity> create(List<Node> nodes) {
        List<Quantity> list = new ArrayList<>();
        for(Node n: nodes){
            list.add(create(n));
        }
        return list;
    }
}

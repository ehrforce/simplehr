package no.dips.ehrcraft.simplehr.rm;

import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Date;
@Slf4j
public class DateConverter {

    public static Date fromDateString(String val){
        if(val == null){
            return null;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date d;
        try {
            d = sdf.parse(val);
            //log.info(String.valueOf(d));
            return d;

        } catch (ParseException e) {

            throw new RuntimeException(e);
        }

    }

    public static Date fromDateTimeString(String val){
        if(val == null){
            return null;
        }

        TemporalAccessor ta = DateTimeFormatter.ISO_DATE_TIME.parse(val);
        Instant i = Instant.from(ta);
        Date d = Date.from(i);
        //log.info(String.valueOf(d));
        return d;
    }
}

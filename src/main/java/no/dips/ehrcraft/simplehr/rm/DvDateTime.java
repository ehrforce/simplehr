package no.dips.ehrcraft.simplehr.rm;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dom4j.Element;
import org.dom4j.Node;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data @Builder
@AllArgsConstructor
@NoArgsConstructor
public class DvDateTime {
    public String value;

    public Date asDate(){
        return DateConverter.fromDateTimeString(value);
    }

    public static DvDateTime fromXML(Node node){
return fromXML((Element)node);
    }
    public static DvDateTime fromXML(Element element){
        String type = element.attributeValue("type");
        String name = element.getName();
        if("time".contentEquals(name) || "origin".contentEquals(name)){
            // THIS IS A HACK TO HANDLE OBSERVATION TIME & ORIGIN
         type = "HACK_TIME";
        }
        if (type == null) {
            throw new RuntimeException("xsi:type is null for element " + element.asXML());
        }

        String path;
        if ("ELEMENT".contentEquals(type)) {
            path = "oe:value/oe:value";
        } else if ("DV_DATE_TIME".contentEquals(type)) {
            path = "oe:value";
        }else if("HACK_TIME".contentEquals(type)){
            path = "oe:value";
        }

        else {
            throw new RuntimeException("Not supported type " + type
            );
        }

        String val = element.selectSingleNode(path).getStringValue();
        return DvDateTime.builder().value(val).build();


    }

    private static List<DvDateTime> fromXML(List<Node> nodes) {
        List<DvDateTime> l = new ArrayList<>();
        for (Node n : nodes) {
            l.add(fromXML(n));
        }
        return l;
    }
}

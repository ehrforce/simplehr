package no.dips.ehrcraft.simplehr.rm;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dom4j.Element;
import org.dom4j.Node;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DvBoolean {

    private String value;

    public boolean asBoolean(){
        if("true".contentEquals(value)){
            return true;
        }else{
            return false;
        }
    }

    public static DvBoolean create(boolean val){
        if(val){
            return DvBoolean.builder().value("true").build();
        }else{
            return DvBoolean.builder().value("false").build();
        }
    }

    public static DvBoolean toBoolean(Node node) {
        return toBoolean((Element) node);
    }

    private static DvBoolean toBoolean(Element element) {
        String type = element.attributeValue("type");
        if (type == null) {
            throw new RuntimeException("xsi:type is null for element" + element.asXML());
        } else {
            if ("ELEMENT".contentEquals(type)) {
                String name = element.selectSingleNode("oe:name/oe:value").getStringValue();
                String value = element.selectSingleNode("oe:value/oe:value").getStringValue();
                return DvBoolean.builder().value(value).build();

            } else if ("DV_BOOLEAN".contentEquals(type)) {
                String val = element.selectSingleNode("oe:value").getStringValue();
                return DvBoolean.builder().value(val).build();
            } else {
                throw new RuntimeException("Unsupported type: " + type);
            }
        }

    }

    public static List<DvBoolean> toBoolean(List<Node> nodes) {
        List<DvBoolean> list = new ArrayList<>();
        for(Node n: nodes){
            list.add(toBoolean(n));
        }
        return list;
    }
}

package no.dips.ehrcraft.simplehr.rm;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dom4j.Element;
import org.dom4j.Node;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DvDate {

    private String value;

    public Date asDate(){
        return DateConverter.fromDateString(value);
    }

    public static DvDate fromXML(Node node) {

        return fromXML((Element) node);
    }

    private static DvDate fromXML(Element element) {
        String type = element.attributeValue("type");
        if (type == null) {
            throw new RuntimeException("xsi:type is null for element " + element.asXML());
        }

        String path;
        if ("ELEMENT".contentEquals(type)) {
            path = "oe:value/oe:value";
        } else if ("DV_DATE".contentEquals(type)) {
            path = "oe:value";
        } else {
            throw new RuntimeException("Not supported type " + type
            );
        }

        String val = element.selectSingleNode(path).getStringValue();
        return DvDate.builder().value(val).build();


    }

    public static List<DvDate> fromXML(List<Node> nodes) {
        List<DvDate> l = new ArrayList<>();
        for (Node n : nodes) {
            l.add(fromXML(n));
        }
        return l;
    }

}

package no.dips.ehrcraft.simplehr.rm;

import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.dom4j.Element;
import org.dom4j.Node;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@Slf4j
public class CodedText {
    private String name;
    private String value;
    private String codeString;
    private String terminology;
    public static CodedText toCodedText(Node element) {
        return toCodedText((Element) element);
    }

    private static CodedText toCodedText(Element element) {
        String type = element.attributeValue("type");
        if(type == null){
            throw new RuntimeException("xsi:type is null for element " + element.asXML());
        }else{
            if("ELEMENT".contentEquals(type)){
                return elementToCodedText(element);

            }else if("DV_CODED_TEXT".contentEquals(type)){
                return codedTextToCodedText(element);

            }else{
                throw new RuntimeException("Unsupported type " + type  );
            }
        }
    }

    private static CodedText codedTextToCodedText(Element element){
        log.debug("codedTextToCodedText" + element.asXML());
        String defCodePath = "oe:defining_code";
        //String name = element.selectSingleNode("oe:name/oe:value").getStringValue();
        String val = element.selectSingleNode("oe:value").getStringValue();
        String code = element.selectSingleNode(defCodePath + "/oe:code_string").getStringValue();
        String term = element.selectSingleNode(defCodePath + "/oe:terminology_id/oe:value").getStringValue();
        return CodedText.builder().value(val).codeString(code).terminology(term).build();
    }
    private static CodedText elementToCodedText(Element element){
        log.debug("elementToCodedText" + element.asXML());
        String defCodePath = "oe:value/oe:defining_code";
        String name = element.selectSingleNode("oe:name/oe:value").getStringValue();
        String val = element.selectSingleNode("oe:value/oe:value").getStringValue();
        String code = element.selectSingleNode(defCodePath + "/oe:code_string").getStringValue();
        String term = element.selectSingleNode(defCodePath + "/oe:terminology_id/oe:value").getStringValue();
        return CodedText.builder().name(name).value(val).codeString(code).terminology(term).build();
    }

    public static CodedText fromCodeString(String atCode){
        return CodedText.builder().codeString(atCode).build();
    }
    public static List<CodedText> toCodedText(List<Node> nodes) {
        List<CodedText> result = new ArrayList<>();
        for(Node n: nodes){
            result.add(toCodedText(n));
        }
        return result;
    }
}

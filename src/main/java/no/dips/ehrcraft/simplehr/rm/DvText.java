package no.dips.ehrcraft.simplehr.rm;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dom4j.Element;
import org.dom4j.Node;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DvText {

    private String value;


    public static DvText fromXML(Node element) {
        return fromXML((Element) element);
    }

    public static DvText fromXML(Element element) {
        String type = element.attributeValue("type");
        if (type == null) {
            throw new RuntimeException("xsi:type is null for element:" + element.asXML());
        } else {
            if ("ELEMENT".contentEquals(type)) {
                String val = element.selectSingleNode("oe:value/oe:value").getStringValue();
                return DvText.builder().value(val).build();
            } else if ("DV_TEXT".contentEquals(type)) {
                String val = element.selectSingleNode("oe:value").getStringValue();
                return DvText.builder().value(val).build();
            } else {
                throw new RuntimeException("Unsupported type " + type);
            }
        }
    }
    public static List<DvText> fromXML(List<Node> nodes){
        List<DvText> list = new ArrayList<>();
        for(Node n: nodes){
            list.add(fromXML(n));
        }
        return list;
    }
}

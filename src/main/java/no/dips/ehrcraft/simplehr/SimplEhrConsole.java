package no.dips.ehrcraft.simplehr;

import lombok.extern.slf4j.Slf4j;
import no.dips.ehrcraft.simplehr.build.reader.FormContent;
import no.dips.ehrcraft.simplehr.build.reader.ZipPathReader;
import no.dips.ehrcraft.simplehr.builder.DtoBuilder;
import org.apache.commons.lang3.Validate;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
@Slf4j
public class SimplEhrConsole {


    private final File formFile;
    private final boolean addConverterAnnotation;
    private final String packageName;
    private final String outDirectory;
    private final SimplerControllerApi api;

    public SimplEhrConsole(File formFile, boolean addConverterAnnotation, String packageName, String outDirectory, SimplerControllerApi api){
        Validate.notNull(formFile, "Form file can not be null");
        Validate.notNull(packageName, "Package name can not be null");
        Validate.notNull(outDirectory, "Out directory can not be null");
        this.formFile = formFile;
                this.addConverterAnnotation = addConverterAnnotation;
        this.packageName = packageName;
        this.outDirectory = outDirectory;
        this.api = api;
    }

    public void run(){

        log.info("Loading form:" + formFile);
        if(formFile != null ||  formFile.exists()){

            try {
                log.info("Loading from file: " + formFile.getAbsolutePath());
                FormContent content = ZipPathReader.read(formFile);
                Path fsRoot = Paths.get(outDirectory);
                DtoBuilder.generateDtoAndConverter(fsRoot, content, packageName, addConverterAnnotation);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            log.warn("The file does not exist");
            api.showHelpAndExit();
        }
    }
}

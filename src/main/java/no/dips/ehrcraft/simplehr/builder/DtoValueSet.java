package no.dips.ehrcraft.simplehr.builder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data @Builder @NoArgsConstructor @AllArgsConstructor
public class DtoValueSet {

    private List<DtoValue> inputList = new ArrayList<>();

    public DtoValueSet addValue(DtoValue value){
        if(inputList == null){
            inputList = new ArrayList<>();
        }
        inputList.add(value);
        return this;
    }

}

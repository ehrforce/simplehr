package no.dips.ehrcraft.simplehr.builder;

import com.squareup.javapoet.*;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import no.dips.ehrcraft.simplehr.OpenEhrConverter;
import no.dips.ehrcraft.simplehr.annotations.Archetype;
import no.dips.ehrcraft.simplehr.annotations.Converter;
import no.dips.ehrcraft.simplehr.annotations.Path;
import no.dips.ehrcraft.simplehr.annotations.RmType;
import no.dips.ehrcraft.simplehr.build.reader.FormContent;

import javax.lang.model.element.Modifier;
import java.io.IOException;
import java.util.List;

@Slf4j
public class DtoBuilder {
    private static String poetString(String s) {
        return "\"" + s + "\"";
    }

    public static String cleanPathName(String name) {
        return StringUtil.clean(name);

    }

    public static void generateDtoAndConverter(java.nio.file.Path toDir, FormContent content, String packageName, boolean addConverterAnnotation) throws IOException {
        DtoClass c = DtoClass.builder()
                .archetype("openEHR-TEST")
                .name(content.getMetadata().getName())
                .paths(content.getDtoPaths())
                .build();
        JavaFile dtoFile = create(packageName, c, addConverterAnnotation);
        dtoFile.writeTo(toDir);
        c.getPaths().stream().forEach(p -> {

            if (p.getValueSet() != null && p.getValueSet().getInputList() != null && p.getValueSet().getInputList().size() > 0) {
                TypeSpec valueEnum = DtoConverterBuilder.createValueTypeEnum(p.getValueSet(), p.getName(), packageName);
                JavaFile valueEnumFile = JavaFile.builder(packageName + ".converter.valuesets", valueEnum).build();
                try {
                    valueEnumFile.writeTo(toDir);
                } catch (IOException e) {
                    log.warn("Error writing valueEnumFile to " + toDir + ", " + e.getMessage());

                }


            }

            JavaFile f = DtoConverterBuilder.build(packageName + ".converter", StringUtil.className(p.getName()), p.getRmType().getClazz(), String.class);


            try {
                f.writeTo(toDir);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });


    }


    public static JavaFile create(String packageName, DtoClass c) {
        return create(packageName, c, false);
    }

    public static JavaFile create(String packageName, DtoClass c, boolean addConverterAnnotation) {

        AnnotationSpec archetype = AnnotationSpec
                .builder(Archetype.class)
                .addMember("archetype", poetString(c.getArchetype()))
                .build();
        TypeSpec.Builder out = TypeSpec.classBuilder(c.getName())
                .addAnnotation(archetype)
                .addAnnotation(Data.class)
                .addAnnotation(Builder.class);
        int n = 0;
        for (DtoPath p : c.getPaths()) {
            n++;
            String name = cleanPathName(p.getName());
            p.setName(name);
            log.trace("Adding field: " + name);
            AnnotationSpec a = AnnotationSpec
                    .builder(Path.class)
                    .addMember("rmType", "$T.$L", RmType.class, p.getRmType().name())
                    .addMember("occurences", "$T.$L", p.getOccurrence().getClass(), p.getOccurrence())
                    .addMember("path", poetString(p.getPath()))
                    .build();
            String converterClassName = StringUtil.className(p.getName()) + DtoConverterBuilder.CLASS_SUFFIX;
            AnnotationSpec converterAnnotation;
            if (RmType.DV_TEXT == p.getRmType()) {
                converterAnnotation = AnnotationSpec.builder(Converter.class)
                        .addMember("converter", OpenEhrConverter.class.getName() + ".class")
                        .build();
            } else if (RmType.DV_DATE_TIME == p.getRmType()) {
                converterAnnotation = AnnotationSpec.builder(Converter.class)
                        .addMember("converter", OpenEhrConverter.class.getName() + ".class")
                        .build();
            } else {
                converterAnnotation = AnnotationSpec.builder(Converter.class)
                        .addMember("converter", packageName + ".converter." + converterClassName + ".class")
                        .build();
            }
            switch (p.getOccurrence()) {
                case SINGLE:
                    FieldSpec.Builder b = FieldSpec.builder(p.getRmType().getClazz(), p.getName())
                            .addModifiers(Modifier.PRIVATE)
                            .addAnnotation(a)
                            .addJavadoc("Feltet blir automatisk generert - og er nummer" + n);
                    if (addConverterAnnotation) {
                        b.addAnnotation(converterAnnotation);
                    }
                    FieldSpec f = b.build();
                    out.addField(f);
                    break;
                case MULTIPLE:
                    FieldSpec.Builder blist = FieldSpec.builder(
                            ParameterizedTypeName.get(List.class, p.getRmType().getClazz()), p.getName()
                    ).addModifiers(Modifier.PRIVATE)
                            .addAnnotation(a);
                    if (addConverterAnnotation) {
                        blist.addAnnotation(converterAnnotation);
                    }
                    FieldSpec flist = blist.build();
                    out.addField(flist);
                    break;
            }

        }
        JavaFile javaFile = JavaFile.builder(packageName, out.build())
                .indent("   ").build();
        return javaFile;

    }
}

package no.dips.ehrcraft.simplehr.builder;

import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Data @Builder
@Slf4j
public class DtoClass {
    private String name;
    private String archetype;
    @Builder.Default
    private List<DtoPath> paths = new ArrayList<>();

    public DtoClass addPath(DtoPath path){
        paths.add(path);
        return this;
    }
}

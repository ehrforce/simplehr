package no.dips.ehrcraft.simplehr.builder;

import com.google.common.base.CaseFormat;
import com.google.common.base.CharMatcher;
import com.google.common.base.Strings;

public class StringUtil {
    public static String clean(String name) {
        
        name = CharMatcher.whitespace().replaceFrom(name, '_');
        name = CharMatcher.anyOf("Ææ").replaceFrom(name, "ae");
        name = CharMatcher.anyOf("Øø").replaceFrom(name, "oe");
        name = CharMatcher.anyOf("Åå").replaceFrom(name, "aa");
        //name = CharMatcher.anyOf("≥").replaceFrom(name, "_");
        name = CharMatcher.javaIsoControl().replaceFrom(name, "_");

        CharMatcher illegal = CharMatcher.whitespace().or(CharMatcher.anyOf("<>:|?*\"/\\")).or(CharMatcher.javaIsoControl()).or(CharMatcher.anyOf("()-.,"));
        name = illegal.trimAndCollapseFrom(name, '_');
        name = name.toUpperCase();
        name = CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, name);
        name = name.replaceAll("[^\\x20-\\x7e]", "");

        return name;
    }
    public static String variableName(String name){
        return clean(name);

    }
    public static String className(String name){
        return CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_CAMEL, clean(name));
    }
}

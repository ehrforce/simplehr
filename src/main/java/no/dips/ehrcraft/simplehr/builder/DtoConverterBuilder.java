package no.dips.ehrcraft.simplehr.builder;

import com.squareup.javapoet.*;
import lombok.extern.slf4j.Slf4j;
import no.dips.ehrcraft.simplehr.OpenEhrConverter;

import javax.lang.model.element.Modifier;

@Slf4j
public class DtoConverterBuilder {
    public static final String CLASS_SUFFIX = "Converter";


    private static final String CONVERTER_CLASS_NAME = "OpenEhrConverter";
    private final String packageName;
    private final String converterClassName;


    /**
     * @param className     typically the attribute name to be converted, will be the class name of the generated converter
     * @param openEHRClass  the openEHR class which carry the data
     * @param externalClass the external class to transform openEHR data into or from
     * @return
     */
    public static final JavaFile build(String packageName, String className, Class openEHRClass, Class externalClass) {
        return build(packageName, className, openEHRClass, externalClass, null);
    }
    public static final JavaFile build(String packageName, String className, Class openEHRClass, Class externalClass, DtoValueSet valueSet) {
        DtoConverterBuilder builder = new DtoConverterBuilder(packageName, CONVERTER_CLASS_NAME);
        return builder.create(className + CLASS_SUFFIX, openEHRClass, externalClass, false, valueSet);
        
    }





    public DtoConverterBuilder(String packageName, String converterClassName) {
        this.packageName = packageName;
        this.converterClassName = converterClassName;
    }
    private JavaFile create(String className, Class openEHRClass, Class externalClass, boolean useThrowStatement, DtoValueSet valueSet) {

        TypeSpec.Builder converter =  create(className,openEHRClass,externalClass,useThrowStatement);
        if(valueSet != null &&  valueSet.getInputList() != null && valueSet.getInputList().size() > 0){
            TypeSpec pathEnum = createValueTypeEnum(valueSet, className, "no.dips.data");
            log.warn("This have to be changed to create a map initializer");

        }
        JavaFile javaFile = JavaFile.builder(packageName, converter.build()).build();
        log.debug(javaFile.toString());
        return javaFile;
    }


    public TypeSpec.Builder create(String className, Class openEHRClass, Class externalClass, boolean useThrowStatement) {
        CodeBlock returnStatement;
        if (useThrowStatement) {
            returnStatement = CodeBlock.builder()
                    .add("throw new $T($S)", RuntimeException.class, "NOT IMPLEMENTED " + className)
                    .build();
        } else {
            returnStatement = CodeBlock.builder()
                    .add("return null")
                    .build();

        }

        MethodSpec constructor = MethodSpec.constructorBuilder()
                .addModifiers(Modifier.PUBLIC)
                .addStatement("super($N,$N)", className + "::fromOpenEhr", className + "::toOpenEhr")
                .build();

        TypeSpec.Builder s = TypeSpec.classBuilder(className)
                .superclass(
                        ParameterizedTypeName.get(ClassName.get(OpenEhrConverter.class), TypeVariableName.get(openEHRClass), TypeVariableName.get(externalClass))

                )
                .addAnnotation(Slf4j.class)
                .addModifiers(Modifier.PUBLIC)
                .addMethod(constructor)
                .addMethod(
                        MethodSpec.methodBuilder("fromOpenEhr")
                                .addModifiers(Modifier.PRIVATE, Modifier.STATIC, Modifier.FINAL)
                                .addParameter(openEHRClass, "value")

                                .beginControlFlow("if(value == null)")
                                .addStatement("return null")
                                .nextControlFlow("else")
                                .addComment("TODO HER MÅ DU IMPLEMENTERE")
                                .addStatement("log.warn($S)", "Not implemented:  " + className)
                                .addStatement(returnStatement)
                                .endControlFlow()

                                .returns(externalClass)
                                .build()
                )
                .addMethod(
                        MethodSpec.methodBuilder("toOpenEhr")
                                .addModifiers(Modifier.PRIVATE, Modifier.STATIC, Modifier.FINAL)
                                .addParameter(externalClass, "value")

                                .beginControlFlow("if(value == null)")
                                .addStatement("return null")

                                .nextControlFlow("else")
                                .addComment("TODO HER MÅ DU IMPLEMENTERE")
                                .addStatement("log.warn($S)", "Not implemented:  " + className)
                                .addStatement(returnStatement)
                                .endControlFlow()

                                .returns(openEHRClass)
                                .build()
                )

                ;
        return s;

    }
    public static final  TypeSpec createValueTypeEnum(DtoValueSet p, String dtoPathName, String packageName) {

        if (p.getInputList() != null && p.getInputList().size() > 0) {
            final TypeSpec.Builder enumS = TypeSpec.enumBuilder(StringUtil.className(dtoPathName + "Enum")
            ).addModifiers(Modifier.PUBLIC);
            p.getInputList().stream().forEach(val -> {
                //log.info(String.valueOf(val));
                //enumS.addEnumConstant(StringUtil.variableName(val.getLabel()));
                enumS.addEnumConstant(
                        "e_" +
                                StringUtil.variableName(val.getLabel()).toUpperCase(),
                        TypeSpec.anonymousClassBuilder("$S, $S", StringUtil.variableName(val.getAtCode()), "targetCode")
                                .addJavadoc(CodeBlock.builder()
                                        .addStatement(val.getAtCode() + " -> " + val.getLabel())
                                        .build())
                                .build()
                );


            });
            enumS.
                    addField(String.class, "atCode", Modifier.PRIVATE, Modifier.FINAL)
                    .addField(String.class, "targetCode", Modifier.PRIVATE, Modifier.FINAL)
                    .addMethod(MethodSpec.constructorBuilder()
                            .addParameter(String.class, "atCode")
                            .addParameter(String.class, "targetCode")
                            .addModifiers(Modifier.PRIVATE)
                            .addCode(CodeBlock.builder()
                                    .addStatement("this.$N = $N", "atCode", "atCode")
                                    .addStatement("this.$N = $N", "targetCode", "targetCode")
                                    .build())
                            .build());

            enumS.addMethod(MethodSpec.methodBuilder("getTargetCode")
                    .addJavadoc(CodeBlock.builder()
                            .addStatement("Implement the correct mapping here")
                            .build())
                    .addModifiers(Modifier.PUBLIC)
                    .returns(String.class)
                    .addCode(CodeBlock.builder()
                            .addStatement("return targetCode")
                            .build())
                    .build());

            enumS.addMethod(MethodSpec.methodBuilder("getAtCode")
                    .addModifiers(Modifier.PUBLIC)
                    .returns(String.class)
                    .addCode(CodeBlock.builder()
                            .addStatement("return atCode")

                            .build())
                    .build());

            return enumS.build();
        } else {
            return null;
        }

    }

}

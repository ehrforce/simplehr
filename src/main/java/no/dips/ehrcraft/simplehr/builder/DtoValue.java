package no.dips.ehrcraft.simplehr.builder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @Builder
@NoArgsConstructor @AllArgsConstructor
public class DtoValue {
    private String atCode;
    private String label;

}

package no.dips.ehrcraft.simplehr.builder;

import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import no.dips.ehrcraft.simplehr.annotations.OccurencesEnum;
import no.dips.ehrcraft.simplehr.annotations.RmType;

@Data @Slf4j @Builder
public class DtoPath {

    private String name;
    private String path;
    private OccurencesEnum occurrence;
    private RmType rmType;
    private DtoValueSet valueSet;
}

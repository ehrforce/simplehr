package no.dips.ehrcraft.simplehr.annotations;

import no.dips.ehrcraft.simplehr.rm.*;

public enum RmType {
    DV_CODED_TEXT(CodedText.class), DV_DATE_TIME(DvDateTime.class), DV_DATE(DvDate.class), DV_QUANTITY(Quantity.class), DV_BOOLEAN(DvBoolean.class), DV_TEXT(DvText.class);

    private final Class clazz;

    private RmType(Class clazz){

        this.clazz = clazz;
    }

    public static boolean isSupported(String val){
        for(RmType t: RmType.values()){
            if(t.name().contentEquals(val)){
                return true;
            }
        }
        return false;

    }
    public Class getClazz() {
        return clazz;
    }
}

package no.dips.ehrcraft.simplehr.annotations;

public enum OccurencesEnum {
    SINGLE, MULTIPLE;
}

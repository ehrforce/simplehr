package no.dips.ehrcraft.simplehr.annotations;

import java.lang.annotation.*;

@Documented
@Target(ElementType.FIELD)
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface Path {
    String path();
    RmType rmType() default RmType.DV_CODED_TEXT;
    OccurencesEnum occurences() default OccurencesEnum.SINGLE;
}

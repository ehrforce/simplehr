package no.dips.ehrcraft.simplehr.annotations;

import java.lang.annotation.*;

@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface Archetype {
    String archetype();

}

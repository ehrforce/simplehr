package no.dips.ehrcraft.simplehr;

import lombok.extern.slf4j.Slf4j;
import no.dips.ehrcraft.simplehr.annotations.Archetype;
import no.dips.ehrcraft.simplehr.annotations.Path;

import java.io.*;
import java.util.Optional;

@Slf4j
public class SimplEhrMapper {

    /**
     * Read the given stream into a XML document and load the class with openEHR annotations
     * @see Path
     * @see Archetype
     * @param xmlStream
     * @param clazz
     * @param <T>
     * @return
     * @throws IOException
     */
    public <T> Optional<T> readValue(InputStream xmlStream, Class<T> clazz) throws IOException{
        DocumentController controller =  DocumentController.create(xmlStream);
        LoadCompositionWithAnnotation loadCompositionWithAnnotation = new LoadCompositionWithAnnotation(controller);
        return loadCompositionWithAnnotation.loadFrom(clazz);
    }

    /**
     * Experimental mapper for file based XML
     * @param xmlFile
     * @param clazz
     * @param <T>
     * @return
     * @throws IOException
     */
    public  <T> Optional<T> readValue(File xmlFile, Class<T> clazz) throws IOException {
        if(xmlFile.exists()){
            try {
                FileInputStream xmlSTream = new FileInputStream(xmlFile);
                return readValue(xmlSTream, clazz);
            } catch (FileNotFoundException e) {
                throw e;
            }
        }else{
            throw new  FileNotFoundException(xmlFile + " was not found");
        }
    }

    /**
     * Experimental mapper for string based XML
     * @param xmlString
     * @param clazz
     * @param <T>
     * @return
     * @throws IOException
     */
    public <T> Optional<T> readValue(String xmlString, Class<T> clazz) throws IOException {
        InputStream targetStream = new ByteArrayInputStream(xmlString.getBytes());
        return readValue(targetStream, clazz);
    }

}

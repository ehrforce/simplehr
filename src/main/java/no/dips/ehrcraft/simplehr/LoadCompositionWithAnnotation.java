package no.dips.ehrcraft.simplehr;

import lombok.extern.slf4j.Slf4j;
import no.dips.ehrcraft.simplehr.annotations.OccurencesEnum;
import no.dips.ehrcraft.simplehr.annotations.Archetype;
import no.dips.ehrcraft.simplehr.annotations.Path;
import no.dips.ehrcraft.simplehr.annotations.RmType;
import no.dips.ehrcraft.simplehr.rm.*;
import org.dom4j.Node;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Optional;

@Slf4j
public class LoadCompositionWithAnnotation {


    private final DocumentController controller;

    public LoadCompositionWithAnnotation(DocumentController controller) {

        this.controller = controller;
    }

    /**
     * Load the class from the document controller.
     *
     * @param clazz
     * @param <T>
     * @return
     */
    public <T> Optional<T> loadFrom(Class<T> clazz) {

        try {
            if (clazz.isAnnotationPresent(Archetype.class)) {
                return Optional.of(loadData(clazz));
            } else {
                return Optional.empty();
            }
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private <T> T loadData(Class<T> clazz) throws IllegalAccessException, InstantiationException {
        long begin = System.currentTimeMillis();
        T instance = clazz.newInstance();
        log.trace("Is openEHR archetype binding");
        Archetype archetypeBinding = clazz.getAnnotation(Archetype.class);
        String archetype = archetypeBinding.archetype();
        String basePath = "/content[" + archetype + "]";
        for (Field f : clazz.getDeclaredFields()) {
            if (f.isAnnotationPresent(Path.class)) {
                Path pb = f.getAnnotation(Path.class);
                String path = pb.path();
                RmType rmtype = pb.rmType();
                log.debug("path:" + path + ", rmtype: " + rmtype);
                OccurencesEnum occurences = pb.occurences();
                String aqlPath = basePath + path;
                f.setAccessible(true);
                switch (rmtype) {
                    case DV_CODED_TEXT:
                        setCodedText(instance, f, occurences, aqlPath);
                        break;
                    case DV_QUANTITY:
                        setQuantity(instance, f, occurences, aqlPath);
                        break;
                    case DV_BOOLEAN:
                        setBoolean(instance, f, occurences, aqlPath);
                        break;


                    case DV_TEXT:
                        setText(instance, f, occurences, aqlPath);
                        break;
                    case DV_DATE_TIME:
                        setDateTime(instance, f, occurences, aqlPath);
                        break;
                    case DV_DATE:
                        setDvDate(instance, f, occurences, aqlPath);
                        break;
                    default:
                        log.warn("Not supported RMTYPE" + rmtype);
                        break;
                }

            }
        }
        long end = System.currentTimeMillis();
        log.debug("Loaded in  " + (end - begin) + " ms");
        ;
        return instance;
    }

    private <T> void setDvDate(T instance, Field f, OccurencesEnum occurences, String aqlPath) throws IllegalAccessException {
        switch (occurences){
            case SINGLE:
                Node n = controller.selectSingleNode(aqlPath);
                if(n != null){
                    f.set(instance, DvDate.fromXML(n));
                }
                break;
            case MULTIPLE:
                List<Node> nodes = controller.selectNodes(aqlPath);
                if(nodes != null){
                    f.set(instance, DvDate.fromXML(nodes));

                }
                break;
        }
    }

    private <T> void setDateTime(T instance, Field f, OccurencesEnum occurences, String aqlPath) throws IllegalAccessException {
        switch (occurences){
            case SINGLE:
                Node n = controller.selectSingleNode(aqlPath);
                if(n != null){
                f.set(instance, DvDateTime.fromXML(n));
                }
                break;
            case MULTIPLE:
                List<Node> nodes = controller.selectNodes(aqlPath);
                if(nodes != null){
                    f.set(instance, nodes);

                }
                break;
        }
    }

    private <T> void setText(T instance, Field f, OccurencesEnum occurences, String aqlPath) throws IllegalAccessException {
        switch (occurences) {
            case SINGLE:
                Node n = controller.selectSingleNode(aqlPath);
                if (n != null) {
                    f.set(instance, DvText.fromXML(n));
                }
                break;
            case MULTIPLE:
                List<Node> ndoes = controller.selectNodes(aqlPath);
                if (ndoes != null) {
                    List<DvText> values = DvText.fromXML(ndoes);
                    f.set(instance, values);
                }
                break;
        }
    }

    private <T> void setBoolean(T instance, Field f, OccurencesEnum occurencesEnum, String aqlPath) throws IllegalAccessException {
        switch (occurencesEnum) {
            case SINGLE:
                Node node = controller.selectSingleNode(aqlPath);
                if (node != null) {
                    DvBoolean b = DvBoolean.toBoolean(node);
                    f.set(instance, b);
                }
                break;
            case MULTIPLE:
                List<Node> nodes = controller.selectNodes(aqlPath);
                if (nodes != null) {
                    List<DvBoolean> values = DvBoolean.toBoolean(nodes);
                    f.set(instance, values);
                }
                break;
        }
    }

    private <T> void setQuantity(T instance, Field f, OccurencesEnum occurences, String aqlPath) throws IllegalAccessException {
        switch (occurences) {
            case SINGLE:
                Node node = controller.selectSingleNode(aqlPath);
                if (node != null) {
                    Quantity q = Quantity.create(node);
                    f.set(instance, q);
                }
                break;
            case MULTIPLE:
                List<Node> nodes = controller.selectNodes(aqlPath);
                if (nodes != null) {
                    List<Quantity> values = Quantity.create(nodes);
                    f.set(instance, values);
                }
                break;
            default:
                log.warn("Unsupported occurences" + occurences);
                break;
        }
    }


    private <T> void setCodedText(T instance, Field field, OccurencesEnum occurencesEnum, String aqlPath) throws IllegalAccessException {

        switch (occurencesEnum) {
            case SINGLE:
                Node node = controller.selectSingleNode(aqlPath);
                if (node != null) {
                    CodedText t = CodedText.toCodedText(node);
                    field.set(instance, t);
                }

                break;
            case MULTIPLE:
                List<Node> nodes = controller.selectNodes(aqlPath);
                if (nodes != null && nodes.size() > 0) {
                    List<CodedText> list = CodedText.toCodedText(nodes);
                    field.set(instance, list);
                }
                break;
            default:
                log.warn("Unsupported occurences" + occurencesEnum);
                break;

        }
    }


}

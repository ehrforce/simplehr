package no.dips.ehrcraft.simplehr;

import lombok.extern.slf4j.Slf4j;
import no.dips.ehrcraft.simplehr.rm.CodedText;
import no.dips.ehrcraft.simplehr.rm.Quantity;
import org.dom4j.Node;

@Slf4j
public class DomHelper {

    public static CodedText toCodedText(Node node) {
        return CodedText.toCodedText(node);
    }

    public static Quantity toQuantity(Node node) {
        return Quantity.create(node);
    }
}

package no.dips.ehrcraft.simplehr;

import org.dom4j.*;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DocumentController {
    private final Document document;
    private final Element root;
    private Logger log = LoggerFactory.getLogger(DocumentController.class);

    public DocumentController(Document document) {
        this.document = document;
        this.root = document.getRootElement();
    }

    public Document getDocument() {
        return document;
    }

    public Node selectSingleNode(String aql) {
        return root.selectSingleNode(AqlXpathHelper.toXpath(aql, true));
    }

    public List<Node> selectNodes(String aql) {
        return root.selectNodes(AqlXpathHelper.toXpath(aql, true));
    }

    public Node selectSingleNode(String aql, Node n) {
        return n.selectSingleNode(AqlXpathHelper.toXpath(aql));
    }

    public List<Node> selectNode(String aql, Node n) {
        return n.selectNodes(AqlXpathHelper.toXpath(aql));
    }

    /**
     * Parse a XML document from given stream
     * @param is
     * @return
     */
    public static DocumentController create(InputStream is) {
        try {
            Document d = createSaxReader().read(is);
            return new DocumentController(d);
        } catch (DocumentException e) {
            throw new RuntimeException(e);
        }

    }

    /**
     * Set up a SAXRead with the default namespaces to be used
     * @return
     */
    private static final SAXReader createSaxReader() {
        Map<String, String> nsContext = new HashMap<>();
        nsContext.put("oe", "http://schemas.openehr.org/v1");
        nsContext.put("xsi", "http://www.w3.org/2001/XMLSchema-instance");
        DocumentFactory.getInstance().setXPathNamespaceURIs(nsContext);
        SAXReader reader = new SAXReader();
        return reader;
    }


}

package no.dips.ehrcraft.simplehr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AqlXpathHelper {
private static Logger log = LoggerFactory.getLogger(AqlXpathHelper.class);

    /**
     * Utility to concert openEHR AQL paths into XPATHS.
     * @param aql
     * @return
     */
    public static String toXpath(String aql) {
        StringBuilder b = new StringBuilder();
        if (aql.startsWith("/")) {
            aql= aql.substring(1);
        }
        String[] arr = aql.split("/");
        for (int i = 0; i < arr.length; i++) {
            String n = arr[i];
            int posStartAtt = n.indexOf("[");
            int posEndAtt = n.indexOf("]");
            if (posStartAtt > 0) {
                String el = "/oe:" + n.substring(0, posStartAtt);
                String attr = n.substring(posStartAtt + 1, posEndAtt);
                String attrString = "[@archetype_node_id='" + attr + "']";
                b.append(el).append(attrString);
            } else {
                b.append("/oe:" + n);
            }

        }
        String result = b.toString();
        log.debug(result);
        return result;
    }

    /**
     * Add a prefix / if XPATH is to be set from root
     * @param aql
     * @param fromRoot
     * @return
     */
    public static String toXpath(String aql, boolean fromRoot){
        if(fromRoot){
            return "/" + toXpath(aql);
        }else{
            return toXpath(aql);
        }

    }
}

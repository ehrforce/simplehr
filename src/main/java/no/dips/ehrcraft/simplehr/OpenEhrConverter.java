package no.dips.ehrcraft.simplehr;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class OpenEhrConverter<T,U> {
    private final Function<T,U> fromOpenEhr;
    private final Function<U,T> toOpenEhr;

    public OpenEhrConverter(final Function<T,U> fromOpenEhr, final Function<U,T> toOpenEhr){
        this.fromOpenEhr = fromOpenEhr;
        this.toOpenEhr = toOpenEhr;
    }

    public final U convertFromOpenEhr(final T openEhrValue){
        return fromOpenEhr.apply(openEhrValue);
    }
    public final T convertToOpenEhr(final U externalValue){
        return toOpenEhr.apply(externalValue);
    }

    public final List<U> createFromOpenEHRs(final Collection<T> openEHRs){
        return openEHRs.stream().map(this::convertFromOpenEhr).collect(Collectors.toList());
    }

    public final List<T> createToOpenEHRs(final Collection<U> kremts){
        return kremts.stream().map(this::convertToOpenEhr).collect(Collectors.toList());
    }

}

package no.dips.ehrcraft.simplehr;

import com.google.common.base.Stopwatch;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.*;

import java.io.File;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

@Slf4j
public class SimplEhr implements SimplerControllerApi {
    private static final Options OPTIONS = new Options();

    private SimplEhr() {

    }

    public static void main(String[] args) {

        SimplEhr simplEhr = new SimplEhr();
        String outDirectory = "tmp/src/main/java";
        String packageName = "no.dips.simplehr.generated";
        boolean addConverterAnnotation = false;

        log.info("SimplEhr started");
        OPTIONS.addOption("form", true, "Path for form");
        OPTIONS.addOption("out", true, "Directory to write generated files to, default:"  + outDirectory);
        OPTIONS.addOption(new Option("c", "converter", false, "Add converter option"));
        OPTIONS.addOption(new Option("p", "package", true, "Define the output package. Default: " + packageName));

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;


        try {
            cmd = parser.parse(OPTIONS, args);
        } catch (ParseException e) {
            log.error(String.format("Error parsing commandline %s", e.getMessage()));
            showHelp();
        }

        if (cmd.hasOption("c")) {
            log.info("Will add converter annotation");
            addConverterAnnotation = true;
        }
        if (cmd.hasOption("h")) {
            showHelp();
        }

        if (!cmd.hasOption("form")) {
            log.warn("form is required attribute");
            showHelp();
        }

        try {
            String form = cmd.getOptionValue("form");
            File f = new File(form);
            SimplEhrConsole console = new SimplEhrConsole(f, addConverterAnnotation, packageName, outDirectory, simplEhr);
            Stopwatch stopwatch = Stopwatch.createStarted();
            console.run();
            stopwatch.stop();

            log.info("time: " + stopwatch); // formatted string like "12.3 ms"
        } catch (RuntimeException e) {
            log.error("Error when running command: ", e);
            e.printStackTrace();
            showHelp();
        }

    }

    private static void showHelp() {
        HelpFormatter formatter = new HelpFormatter();

        formatter.printHelp("java -jar simplehr<version>.jar ", OPTIONS);
        log.info("Finished");

        System.exit(0);
    }

    @Override
    public void showHelpAndExit() {
        SimplEhr.showHelp();

    }
}

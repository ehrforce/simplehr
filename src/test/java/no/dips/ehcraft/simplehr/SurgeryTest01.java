package no.dips.ehcraft.simplehr;

import no.dips.ehrcraft.simplehr.DocumentController;
import no.dips.ehrcraft.simplehr.LoadCompositionWithAnnotation;
import no.dips.ehrcraft.simplehr.rm.DvBoolean;
import no.dips.ehrcraft.simplehr.rm.DvText;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;

public class SurgeryTest01 {

    @Test
     void testLoad(){
        DocumentController controller = DocumentController.create(CompositionLoader.getSurgery01());
        LoadCompositionWithAnnotation loader = new LoadCompositionWithAnnotation(controller);
        Optional<SurgeryColoRectalBinding> binding = loader.loadFrom(SurgeryColoRectalBinding.class);
        assertTrue(binding.isPresent());
        SurgeryColoRectalBinding b = binding.get();
        DvBoolean val = b.getPatology();
        assertNotNull(val, "DvBoolean for patology is null");
        assertEquals("true", val.getValue());

        DvText specifySurgery = b.getSpecifySurgery();
        assertNotNull(specifySurgery, "DvText for spesifiser ingrep er null");
        assertEquals("Annet", specifySurgery.getValue());

    }

}

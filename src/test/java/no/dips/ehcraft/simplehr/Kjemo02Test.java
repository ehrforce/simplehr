package no.dips.ehcraft.simplehr;
import no.dips.ehrcraft.simplehr.DocumentController;
import no.dips.ehrcraft.simplehr.LoadCompositionWithAnnotation;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
public class Kjemo02Test {

    @Test
    public void testDataIsCorrect(){
        DocumentController controller = DocumentController.create(CompositionLoader.getKjemo02());
        LoadCompositionWithAnnotation loader = new LoadCompositionWithAnnotation(controller);
        Optional<OpenEhrColoKjemoBinding> resultOptional = loader.loadFrom(OpenEhrColoKjemoBinding.class);
        assertTrue(resultOptional.isPresent());
        OpenEhrColoKjemoBinding result = resultOptional.get();


        assertEquals(12.0, result.getCea().getMagnitude());
    }


}

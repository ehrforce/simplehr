package no.dips.ehcraft.simplehr;

import lombok.Data;
import no.dips.ehrcraft.simplehr.annotations.Archetype;
import no.dips.ehrcraft.simplehr.annotations.Path;
import no.dips.ehrcraft.simplehr.annotations.RmType;
import no.dips.ehrcraft.simplehr.rm.DvBoolean;
import no.dips.ehrcraft.simplehr.rm.DvDateTime;
import no.dips.ehrcraft.simplehr.rm.DvText;

@Data
@Archetype(archetype = "openEHR-EHR-OBSERVATION.kreftmelding_kolorektal_kirurgi_dips.v1")
public class SurgeryColoRectalBinding {

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.kreftmelding_kolorektal_patologilaboratorium_dips.v1]/items[at0027]", rmType = RmType.DV_BOOLEAN)
    private DvBoolean patology;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[at0176]/items[at0184]", rmType = RmType.DV_TEXT)
    private DvText specifySurgery;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[at0176]/items[at0178]", rmType = RmType.DV_DATE)
    private DvDateTime surgeryDate;
}

package no.dips.ehcraft.simplehr;

import java.io.InputStream;

public class CompositionLoader {

    public static InputStream getKjeamo01(){
        return CompositionLoader.class.getResourceAsStream("/kreftreg/kjemo/kjemo01.xml");
    }
    public static InputStream getKjemo02(){
        return CompositionLoader.class.getResourceAsStream("/kreftreg/kjemo/kjemo02.xml");
    }
    public static InputStream getSurgery01(){
        return CompositionLoader.class.getResourceAsStream("/kreftreg/kjemo/surgery01.xml");
    }
}

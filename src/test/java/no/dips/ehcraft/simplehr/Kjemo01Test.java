package no.dips.ehcraft.simplehr;

import no.dips.ehrcraft.simplehr.DocumentController;
import no.dips.ehrcraft.simplehr.LoadCompositionWithAnnotation;
import no.dips.ehrcraft.simplehr.rm.CodedText;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;


public class Kjemo01Test {

@Test
    @RepeatedTest(5)
    public void testDataIsCorrect() {
        DocumentController controller = DocumentController.create(CompositionLoader.getKjeamo01());
        LoadCompositionWithAnnotation loader = new LoadCompositionWithAnnotation(controller);
        Optional<OpenEhrColoKjemoBinding> optionalResult = loader.loadFrom(OpenEhrColoKjemoBinding.class);
        assertTrue(optionalResult.isPresent());
        OpenEhrColoKjemoBinding result = optionalResult.get();
        assertNotNull(result);
        assertEquals("at0340", result.getBehandlingFor().getCodeString());
        assertEquals("at0002", result.getLokalisering().getCodeString());
        assertEquals("at0008", result.getSpecificLocation().getCodeString());
        assertEquals("at0002", result.getEcogStatus().getCodeString());
        assertEquals("0 - Full daglig aktivitet", result.getEcogStatus().getValue());

        assertEquals("at0418", result.getReasonForChangedTreatment().get(0).getCodeString());

        List<CodedText> medications = result.getMedication();
        assertEquals(2, medications.size());
        assertEquals("at0387", medications.get(0).getCodeString());
        assertEquals("at0391", medications.get(1).getCodeString());


        assertNotNull(result.getObservationTime());
        assertNotNull(result.getObservationTime().getValue());
        assertNotNull(result.getObservationTime().asDate());
    }
}

package no.dips.ehcraft.simplehr;

import com.google.common.base.CaseFormat;
import no.dips.ehrcraft.simplehr.builder.StringUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;
public class StringFormatTest {

    @ParameterizedTest
    @CsvSource(value = {
            "weight,Weight",
            "w eight,WEight",
            "ørn,Oern",
            "test var,TestVar",
            "Angir avstand fra analåpning til nedre kant av tumor (målt på stivt skop),AngirAvstandFraAnalaapningTilNedreKantAvTumorMaaltPaaStivtSkop"
    })
    public void testToClassName(String name, String facit){

        String result = StringUtil.className(name);
        assertEquals(facit, result);

    }
    @ParameterizedTest
    @CsvSource(value = {
            "weight,weight",
            "w eight,wEight",
            "ørn,oern",
            "test var,testVar",
            "Angir avstand fra analåpning til nedre kant av tumor (målt på stivt skop),angirAvstandFraAnalaapningTilNedreKantAvTumorMaaltPaaStivtSkop"
    })
    public void testToVariableName(String name, String facit){
      assertEquals(facit, StringUtil.variableName(name));
    }
}

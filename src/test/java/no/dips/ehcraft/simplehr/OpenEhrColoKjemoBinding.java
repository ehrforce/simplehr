package no.dips.ehcraft.simplehr;

import lombok.Data;
import no.dips.ehrcraft.simplehr.OpenEhrConverter;
import no.dips.ehrcraft.simplehr.annotations.*;
import no.dips.ehrcraft.simplehr.rm.CodedText;
import no.dips.ehrcraft.simplehr.rm.DvBoolean;
import no.dips.ehrcraft.simplehr.rm.DvDateTime;
import no.dips.ehrcraft.simplehr.rm.Quantity;

import java.util.List;

@Data
@Archetype(archetype = "openEHR-EHR-OBSERVATION.kreftmelding_kolorektalkreft_kjemoterapi_dips.v1")
public class OpenEhrColoKjemoBinding {

    @Path(path = "/data[at0001]/events[at0002]/time", rmType = RmType.DV_DATE_TIME)
    @Converter(converter = OpenEhrConverter.class)
    private DvDateTime observationTime;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[at0337]/items[at0339]")
    private CodedText behandlingFor;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[at0337]/items[at0402]", occurences = OccurencesEnum.MULTIPLE)
    private List<CodedText> lokaliseringMetastaserList;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.kreftmelding_kolorektal_lokalisasjon_av_primaertumor_dips.v1]/items[at0001]/")
    private CodedText lokalisering;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.kreftmelding_kolorektal_lokalisasjon_av_primaertumor_dips.v1]/items[at0004]")
    private CodedText specificLocation;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[at0354]/items[at0355]")
    private CodedText purposeWithTreatment;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[at0354]/items[openEHR-EHR-CLUSTER.kreftmelding_kolorektal_ecog_funksjonsstatus_dips.v1]/items[at0001]")
    private CodedText ecogStatus;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[at0354]/items[openEHR-EHR-CLUSTER.kreftmelding_kolorektal_cea_dips.v1]/items[at0001]/", rmType = RmType.DV_QUANTITY)
    private Quantity cea;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[at0354]/items[openEHR-EHR-CLUSTER.kreftmelding_kolorektal_cea_dips.v1]/items[at0002]/", rmType = RmType.DV_BOOLEAN)
    private DvBoolean ceaTaken;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[at0354]/items[at0369]", rmType = RmType.DV_DATE_TIME)
    private DvDateTime startTreatment;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[at0354]/items[at0370]")
    private CodedText treatmentLine;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[at0354]/items[at0381]/items[at0417]", occurences = OccurencesEnum.MULTIPLE)
    private List<CodedText> reasonForChangedTreatment;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[at0354]/items[at0380]/items[at0382]", occurences = OccurencesEnum.MULTIPLE)
    private List<CodedText> medication;
}

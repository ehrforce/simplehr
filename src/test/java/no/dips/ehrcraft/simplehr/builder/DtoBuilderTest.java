package no.dips.ehrcraft.simplehr.builder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.squareup.javapoet.*;
import lombok.Builder;
import lombok.Data;
import no.dips.ehrcraft.simplehr.annotations.Archetype;
import no.dips.ehrcraft.simplehr.annotations.OccurencesEnum;
import no.dips.ehrcraft.simplehr.annotations.Path;
import no.dips.ehrcraft.simplehr.annotations.RmType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.lang.model.element.Modifier;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

class DtoBuilderTest {

    private DtoClass build() {
        DtoClass c = DtoClass.builder().archetype("openEHR-EHR-OBSERVATION.body_weight.v1")
                .name("BodyWeight")
                .build();

        c.addPath(DtoPath.builder()
                .rmType(RmType.DV_CODED_TEXT)
                .occurrence(OccurencesEnum.SINGLE)
                .name("place")
                .valueSet(DtoValueSet.builder()
                        .inputList(Lists.newArrayList(
                                DtoValue.builder().atCode("at100").label("MyLabelOne").build(),
                                DtoValue.builder().atCode("at101").label("MyLabelTwo").build()
                        ))
                        . build())
                .build());
        c.addPath(DtoPath.builder()
                .name("weight")
                .path("/data/items[at0001")
                .occurrence(OccurencesEnum.SINGLE)
                .rmType(RmType.DV_QUANTITY)
                .build());
        c.addPath(DtoPath.builder().name("multiple")
                .path("/data/items[at0002]")
                .occurrence(OccurencesEnum.MULTIPLE)
                .rmType(RmType.DV_TEXT)
                .build());
        return c;
    }

    @Test
    public void testToJson() throws IOException {
        DtoClass c = build();
        ObjectMapper m = new ObjectMapper();
        StringWriter w = new StringWriter();
        m.writeValue(w, c);
        //System.out.println(w);
        Assertions.assertNotNull(w);
    }



    @Test
    public void testGeneateClass() {

        JavaFile weightType = DtoBuilder.create("no.dips.test", build());
        Assertions.assertNotNull(weightType);
        //System.out.println(weightType.toString());
    }

}
package no.dips.ehrcraft.simplehr.builder;

import com.squareup.javapoet.JavaFile;
import no.dips.ehrcraft.simplehr.rm.CodedText;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DtoConverterBuilderTest {

    @Test
    public void testGenerateConverter(){
        JavaFile result = DtoConverterBuilder.build("no.dips.test","PrimaryTumor", CodedText.class, String.class);
        assertNotNull(result);
        //System.out.println(result.toString());
    }

}
package no.dips.ehrcraft.simplehr.rm;

import lombok.val;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class DvDateTimeTest extends AbstractDomTestBase {
    private Element createElement(String val) {
        return createElement("DEFAULT_NAME", val);
    }

    private Element createElement(String name, String val) {
        Element root = DocumentHelper
                .createElement(oe("items"))
                .addAttribute(xsi("type"), "ELEMENT");
        root
                .addElement(oe("name")).addElement(oe("value"))
                .setText(name);
        root.add(createValue(val));
        return root;
    }

    private Element createValue(String val) {
        Element root = DocumentHelper
                .createElement(oe("value"))
                .addAttribute(xsi("type"), "DV_DATE_TIME");
        root
                .addElement(oe("value"))
                .setText(val);
        return root;
    }

    @Test
    void asDate() {
        String datetime = "2020-05-03T21:27:19.0981875+02:00";
        val dt = DvDateTime.builder().value(datetime).build();
        assertNotNull(dt);
        assertNotNull(dt.asDate());
    }

    @Test
    void fromXML() {
        String datetime = "2020-05-03T21:27:19.0981875+02:00";
        Element e = createElement(datetime);
        //System.out.println(e.asXML());
        assertNotNull(e);
        DvDateTime d = DvDateTime.fromXML(e);
        assertNotNull(d);
        assertEquals(datetime, d.getValue());
    }

    @Test
    void fromXML1() {
    }
}
package no.dips.ehrcraft.simplehr.rm;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CodedTextTest {

    @Test
    void fromCodeString() {
        String atcode = "at001";
        CodedText t = CodedText.fromCodeString(atcode);
        assertNotNull(t);
        assertEquals(atcode, t.getCodeString());
    }
}
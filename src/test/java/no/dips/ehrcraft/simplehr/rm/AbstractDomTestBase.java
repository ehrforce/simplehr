package no.dips.ehrcraft.simplehr.rm;

import org.dom4j.DocumentHelper;
import org.dom4j.QName;

import static no.dips.ehrcraft.simplehr.rm.XmlConstants.OPENEHR_NAMESPACE;
import static no.dips.ehrcraft.simplehr.rm.XmlConstants.XSI_NAMESPACE;

public class AbstractDomTestBase {
    protected QName xsi(String name){
        return DocumentHelper.createQName(name, XSI_NAMESPACE);
    }

    protected QName oe(String name){
        return DocumentHelper.createQName(name, OPENEHR_NAMESPACE);
    }
}

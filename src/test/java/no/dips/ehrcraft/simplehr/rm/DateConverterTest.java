package no.dips.ehrcraft.simplehr.rm;

import org.junit.jupiter.api.RepeatedTest;

import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class DateConverterTest {

    @RepeatedTest( 10)
    public void testGetDateVal(){
        String s = "2020-05-03";
        Date d = DateConverter.fromDateString(s);
        assertNotNull(d);
    }

    @RepeatedTest(10)
    public void testDateTimeVal(){
        String s = "2020-05-03T21:27:19.0981875+02:00";
        Date d = DateConverter.fromDateTimeString(s);
        assertNotNull(d);
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        assertEquals(2020, c.get(Calendar.YEAR));
        assertEquals(5 - 1, c.get(Calendar.MONTH));
        assertEquals(21, c.get(Calendar.HOUR_OF_DAY));
        assertEquals(27, c.get(Calendar.MINUTE));
    }

}
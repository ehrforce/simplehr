package no.dips.ehrcraft.simplehr.rm;

import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class DvDateTest extends AbstractDomTestBase{
    private Element createElement(String value){
        return createElement("DEFAULT_ELEMENT_NAME", value);
    }
    private Element createElement(String name, String value){
        Element root = DocumentHelper
                .createElement(oe("items"))
                .addAttribute(xsi("type"), "ELEMENT");
        root.addElement(oe("name")).addElement(oe("value")).setText(name);
        root.add(createValue(value));
        return root;
    }
    private Element createValue(String value){
        Element rot = DocumentHelper
                .createElement(oe("value"));
        rot.addElement(oe("value")).setText(value);
        return rot;
    }

    @Test
    void asDate() {
        String d = "2020-05-03";
        DvDate dvDate = DvDate.builder().value(d).build();
        Date date = dvDate.asDate();
        assertNotNull(date);
    }

    @Test
    void fromXML() {
        String d = "2020-05-03";
        Element el = createElement(d);
        //System.out.println(el.asXML());
        DvDate dvDate = DvDate.fromXML(el);
        assertNotNull(dvDate);
        assertEquals(d, dvDate.getValue());
    }

    @Test
    void fromXML1() {
    }
}
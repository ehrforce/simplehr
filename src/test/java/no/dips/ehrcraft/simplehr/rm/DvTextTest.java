package no.dips.ehrcraft.simplehr.rm;

import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


class DvTextTest extends AbstractDomTestBase {

    private Element createTextElement(String value) {
        return createTextElement("DEFAULT_NAME", value);
    }

    private Element createTextElement(String elmentName, String value) {
        Element root = DocumentHelper.createElement(oe("items"));
        root.addAttribute(xsi("type"), "ELEMENT");
        root.addElement(oe("name")).addElement(oe("value")).setText(elmentName);
        root.add(creatTextValueElement(value));
        return root;

    }

    private Element creatTextValueElement(String value) {
        Element root = DocumentHelper.createElement(oe("value"));
        root.addAttribute(xsi("type"), "DV_TEXT");
        root.addElement(oe("value")).setText(value);
        return root;

    }

    @Test
    void fromXML() {
        Element t = createTextElement("Annet");
        DvText text = DvText.fromXML(t);
        assertNotNull(text);
        assertEquals("Annet", text.getValue());

    }

    @Test
    void fromXML1() {
        Element t = creatTextValueElement("OjOj");
        DvText text = DvText.fromXML(t);
        assertNotNull(text);
        assertEquals("OjOj", text.getValue());
    }

    @Test
    void fromXML2() {
        List<Node> nodes = new ArrayList<>();
        nodes.add(createTextElement("E1"));
        nodes.add(createTextElement("E2"));
        List<DvText> result = DvText.fromXML(nodes);
        assertNotNull(result);
        assertEquals(2, result.size());
        assertEquals("E1", result.get(0).getValue());
        assertEquals("E2", result.get(1).getValue());
    }
}
package no.dips.ehrcraft.simplehr.rm;


import org.dom4j.Namespace;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class XmlConstants {

    public static final Map<String,String> NAMESPACE_CONTEXT;
    public static final Namespace OPENEHR_NAMESPACE;
    public static final Namespace XSI_NAMESPACE;
    static {
        Map<String, String> nsContext = new HashMap<>();
        nsContext.put("oe", "http://schemas.openehr.org/v1");
        nsContext.put("xsi", "http://www.w3.org/2001/XMLSchema-instance");
        NAMESPACE_CONTEXT = Collections.unmodifiableMap(nsContext);

        Namespace openEHR = new Namespace("oe", nsContext.get("oe"));
        OPENEHR_NAMESPACE = openEHR;

        Namespace xsi = new Namespace("xsi", nsContext.get("xsi"));
        XSI_NAMESPACE = xsi;



    }
}

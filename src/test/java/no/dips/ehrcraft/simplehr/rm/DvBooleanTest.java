package no.dips.ehrcraft.simplehr.rm;

import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DvBooleanTest  extends  AbstractDomTestBase{

    private Element createDvBooleanElement(String name_value, String bool_value) {
        Element el = DocumentHelper.createElement(oe("name"));
        el.addAttribute(xsi("type"), "ELEMENT");
        el.addAttribute(oe("archetype_node_id"), "at0001");
        el.addElement(oe("name"))
                .addElement(oe("value"))
                .addText(name_value)
        ;

        el.add(createDvBooleanValueElement(bool_value));
        return el;
    }
    private Element createDvBooleanValueElement(String boolvalue){
        Element bel = DocumentHelper.createElement(oe("value"));
        bel.addAttribute(xsi("type"), "DV_BOOLEAN");
        Element val = DocumentHelper.createElement(oe("value"));
        val.setText(boolvalue);

        bel.add(val);
        return bel;
    }
    @ParameterizedTest
    @ValueSource(strings = {"true", "false"})
    void testAsBoolean(String val){
        DvBoolean result = DvBoolean.builder().value(val).build();

        if("true".contentEquals(val)){
            assertTrue(result.asBoolean());
        }else{
            assertFalse(result.asBoolean());
        }

    }
    @Test
    void testFromBoolean(){
        DvBoolean valTrue = DvBoolean.create(true);
        assertEquals("true", valTrue.getValue());

        DvBoolean valFalse = DvBoolean.create(false);
        assertEquals("false", valFalse.getValue());
    }

    @ParameterizedTest
    @ValueSource(strings = {"true", "false"})
    void toBoolean(String val) {
        Element el = createDvBooleanElement("Er det i samme operasjon gjort inngrep mot metastase(r)", val);
        //System.out.println(el.asXML());

        DvBoolean dvBoolean = DvBoolean.toBoolean(el);
        assertEquals(val, dvBoolean.getValue());
    }

    @ParameterizedTest
    @ValueSource(strings = {"true", "false"})
    void testFromBooleanValue(String val){
        DvBoolean result = DvBoolean.toBoolean(createDvBooleanValueElement(val));
        assertEquals(val, result.getValue());
    }
}
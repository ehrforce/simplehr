package no.dips.ehrcraft.simplehr.generated;

import java.util.List;
import lombok.Builder;
import lombok.Data;
import no.dips.ehrcraft.simplehr.annotations.Archetype;
import no.dips.ehrcraft.simplehr.annotations.OccurencesEnum;
import no.dips.ehrcraft.simplehr.annotations.Path;
import no.dips.ehrcraft.simplehr.annotations.RmType;
import no.dips.ehrcraft.simplehr.rm.DvText;
import no.dips.ehrcraft.simplehr.rm.Quantity;

@Archetype(
        archetype = "openEHR-EHR-OBSERVATION.body_weight.v1"
)
@Data
@Builder
public class BodyWeight {
    @Path(
            rmType = RmType.DV_QUANTITY,
            occurences = OccurencesEnum.SINGLE,
            path = "null"
    )
    private Quantity weight;

    @Path(
            rmType = RmType.DV_TEXT,
            occurences = OccurencesEnum.MULTIPLE,
            path = "null"
    )
    private List<DvText> multiple;
}


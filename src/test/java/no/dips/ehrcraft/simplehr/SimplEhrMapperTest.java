package no.dips.ehrcraft.simplehr;

import no.dips.ehcraft.simplehr.OpenEhrColoKjemoBinding;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class SimplEhrMapperTest {

    @Test
    void readValueFromInputStream() throws IOException {
        InputStream is = SimplEhrMapperTest.class.getResourceAsStream("/kreftreg/kjemo/kjemo01.xml");
        SimplEhrMapper mapper = new SimplEhrMapper();


            Optional<OpenEhrColoKjemoBinding> result =   mapper.readValue(is, OpenEhrColoKjemoBinding.class);
            assertTrue(result.isPresent());
            assertNotNull(result.get());

    }

    @Test
    void readValueFromFile() {
    }

    @Test
    void readValueFromString() {
    }
}
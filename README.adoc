
= SimplEHR

== What is it?

The need was to transform openEHR data into messaging formats, i.e. to the national Cancer Registry. We wanted a way to do the transformation easy, quick and with good quality.

What we want to achieve is: 

image::doc/logo.png[]

SimplEHR transforms openEHR compositions into plain java objects with primitive data values. These are used to convert data into the downstream message.

Example of a class definition is given below.

[source,java]
----
@Archetype(archetype = "openEHR-EHR-OBSERVATION.kreftmelding_kolorektalkreft_kjemoterapi_dips.v1")
public class OpenEhrColoKjemoBinding {

    @Path(path = "/data[at0001]/events[at0002]/time", rmType = RmType.DV_DATE_TIME)
    private DvDateTime observationTime;

    @Path(path = "/data[at0001]/events[at0002]/data[at0003]/items[at0337]/items[at0339]")
    private CodedText behandlingFor;

}

----

WARNING:: This is a prototype developed to explore possibilites, patterns and features of such a tool. It currently support only a few openEHR RM types and a very limited set of path based combinations. 

== Supported RM types

[source,java]
----
public enum RmType {
    DV_CODED_TEXT, DV_DATE_TIME, DV_DATE, DV_QUANTITY, DV_BOOLEAN, DV_TEXT;
}

----

== Getting started 

[source]
----
mvn clean package 

java -jar .\target\simplehr-1.0-SNAPSHOT.jar -c -form .\src\test\resources\kreftreg\forms\Kreftmelding_Kolorektal_kirurgi-2.0.0.zip

.\init_skeleton.bat

cd tmp

mvn compile

----

== Build
SimplEHR is a straight forward maven project. 

[source]
----
mvn package
----

== Use 

.How to run
[source, bash]
----
java -jar .\target\simplehr-1.0-SNAPSHOT.jar 
----
.Description of options
[cols="1,15", options="header"]
|====
|Option | Description
|`form` | The form to be read and is used to extract the openEHR RM types with corresponding paths. 
|`c` | Set this flag to generated converter classes for each element. 
|`out`| Define the directory where the generated classes will be written 
|`p` | Define the java package for the generated code 
|====
